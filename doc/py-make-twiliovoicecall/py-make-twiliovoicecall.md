# ref
https://www.twilio.com/docs/voice/tutorials/how-to-make-outbound-phone-calls-python


# required
a voice-capable Twilio phone number   https://www.twilio.com/docs/voice/tutorials/how-to-make-outbound-phone-calls-python#purchase-a-voice-enabled-twilio-phone-number
Twilio Python SDK                     https://www.twilio.com/docs/libraries/python
your account credentials              https://www.twilio.com/docs/voice/tutorials/how-to-make-outbound-phone-calls-python#retrieve-your-twilio-account-credentials


# req @ a voice-capable Twilio phone number
https://www.twilio.com/docs/voice/tutorials/how-to-make-outbound-phone-calls-python#purchase-a-voice-enabled-twilio-phone-number

buy here https://www.twilio.com/console/phone-numbers/search
difference bw. national number vs mobile number

sample number and charging $7 immediately, and $7/monthly later
![view snapshot](./buy-a-number-tocall.png)

there is a trial balance [displayed in console](https://www.twilio.com/console) and we can buy ![trial number](./trial-number.png) to use it

to make call to your number to test, the number must be verified [view guide](https://www.twilio.com/docs/usage/tutorials/how-to-use-your-free-trial-account#verify-your-personal-phone-number)
ie go to [this console page](https://www.twilio.com/console/phone-numbers/verified), choose TEXT ME
TODO the sms never come to my phone, why?

international call requires granted permission
"Account not authorized to call +6593899125. Perhaps you need to enable some international permissions"
ref. https://stackoverflow.com/a/44190163/248616
check/select countries in http://twilio.com/console/voice/settings/geo-permissions

all call status aka call log https://support.twilio.com/hc/en-us/articles/223132547-What-are-the-Possible-Call-Statuses-and-What-do-They-Mean-

view purchased phone number [here](https://www.twilio.com/console/phone-numbers)

# req @ your account credentials              https://www.twilio.com/docs/voice/tutorials/how-to-make-outbound-phone-calls-python#retrieve-your-twilio-account-credentials
Open in console https://www.twilio.com/console


# req @ Twilio Python SDK
we better learn fast via a [quickstart guide](https://www.twilio.com/docs/voice/quickstart/python)
though, it's easier to define call content by using TwiML, sample code at https://www.twilio.com/docs/voice/tutorials/how-to-make-outbound-phone-calls-python
```python
# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'ACbc6cee67f570c9bd0636ae9b345bdb08'
auth_token  = 'your_auth_token'
client      = Client(account_sid, auth_token)

call = client.calls.create(
    twiml = '<Response><Say>Ahoy, World!</Say></Response>',
    to    = '+14155551212',
    from_ = '+15017122661'
)

print(call.sid)
