#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

    cd "$AH/quickstart"
        MODE='test_make_call_until_answered__run_in_background__multi_number' \
            PYTHONPATH=$AH  pipenv run python "$AH/quickstart/src/make_call_until_answered.py"
