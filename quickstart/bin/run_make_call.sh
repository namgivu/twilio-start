#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

    cd "$AH/quickstart"
        PYTHONPATH=$AH  pipenv run python "$AH/quickstart/src/make_call.py"
