from dotenv import load_dotenv
import os

from quickstart.src.make_call_until_answered import make_call_until_answered__run_in_background


def test_make_call_until_answered__run_in_background():
    load_dotenv()

    make_call_until_answered__run_in_background(
        message = os.environ.get('MESSAGE_TO_SPEAK') + '. 1st call',
        to      = os.environ.get('DEMO_NUMBER_to'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )


def test_make_call_until_answered__run_in_background__multi_number():
    """
    NOTE if redial not working, reduce the quickstart/src/make_call_until_answered.py:BUSY_RESULT_THRESHOLD
    """
    load_dotenv()

    print(f"Calling {os.environ.get('DEMO_NUMBER_to2')} ...")
    make_call_until_answered__run_in_background(
        message = os.environ.get('MESSAGE_TO_SPEAK') + '. 1st call',
        to      = os.environ.get('DEMO_NUMBER_to2'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )

    print(f"Calling {os.environ.get('DEMO_NUMBER_to')} ...")
    make_call_until_answered__run_in_background(
        message = os.environ.get('MESSAGE_TO_SPEAK') + '. 1st call',
        to      = os.environ.get('DEMO_NUMBER_to'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )
