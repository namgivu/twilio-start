from dotenv import load_dotenv
import os

from quickstart.src.make_call import make_call, make_call__run_in_background


def test_make_call():
    load_dotenv()
    make_call(
        message = os.environ.get('MESSAGE_TO_SPEAK'),
        to      = os.environ.get('DEMO_NUMBER_to'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )


#region test_make_call__run_in_background
def test_make_call__run_in_background():
    load_dotenv()

    make_call__run_in_background(
        message = os.environ.get('MESSAGE_TO_SPEAK') + '. 1st call',
        to      = os.environ.get('DEMO_NUMBER_to'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )

def test_make_call__run_in_background__multi_number():
    load_dotenv()

    print(f"Calling {os.environ.get('DEMO_NUMBER_to')} ...")
    make_call__run_in_background(
        message = os.environ.get('MESSAGE_TO_SPEAK') + '. 1st call',
        to      = os.environ.get('DEMO_NUMBER_to'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )

    print(f"Calling {os.environ.get('DEMO_NUMBER_to2')} ...")
    make_call__run_in_background(
        message = os.environ.get('MESSAGE_TO_SPEAK') + '. 2nd call',
        to      = os.environ.get('DEMO_NUMBER_to2'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )
#endregion test_make_call__run_in_background
