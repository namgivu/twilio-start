import json
import sys

from quickstart.src.service.twilio import get_twilio_client


def to_dict(c:'twilio call'):
    return {'sid': c.sid, 'status': c.status, 'from': c.from_, 'to': c.to, }


def get_all():
    client = get_twilio_client()
    calls = client.calls.list(limit=sys.maxsize)  # ref. https://www.twilio.com/docs/voice/tutorials/how-to-retrieve-call-logs-python
    r = [to_dict(c) for c in calls]
    return r


def get_by_sid(sid):
    """
    ref. https://www.twilio.com/docs/voice/tutorials/how-to-retrieve-call-logs-python?code-sample=code-retrieve-call-by-id-example&code-language=Python&code-sdk-version=6.x
    """
    client = get_twilio_client()
    c = client.calls(sid).fetch()
    r = to_dict(c)
    return r


if __name__ == '__main__':
    r = get_all()
    print(json.dumps(r, indent=2, sort_keys=False))
    print(f'{len(r)} called fetched')

    c0 = r[0]
    print(); print(json.dumps(c0, indent=2, sort_keys=False))
    c = get_by_sid(sid=c0['sid'])
    print(c)
