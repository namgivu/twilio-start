import os
from twilio.rest import Client
from dotenv import load_dotenv


def get_twilio_client():
    load_dotenv()

    # Your Account Sid and Auth Token from twilio.com/console
    # DANGER! This is insecure. See http://twil.io/secure
    account_sid = os.environ.get('TWILIO_ACCOUNT_SID')
    auth_token  = os.environ.get('TWILIO__AUTH_TOKEN')

    client = Client(account_sid, auth_token)

    return client
