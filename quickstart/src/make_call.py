import time
from datetime import datetime
import threading

from quickstart.src.service.twilio import get_twilio_client
from quickstart.src import get_call_log


def make_call(message, to, from_, loop=0):
    """
    repeat message until hang-up via :loop=0 ref. https://stackoverflow.com/a/46876407/248616 - all Say attribute ref. https://www.twilio.com/docs/voice/twiml/say#attributes
    TODO pause before repeat ref. https://stackoverflow.com/q/63965283/248616

    call status    queued --> ringing --> in-progress --> completed

    real scenario                                         status from api    status displayed on web console @ call log https://www.twilio.com/console/voice/calls/logs
    -------------                                         ---------------    ------------------------------------------
           answer  .          .           .               completed          Completed
           reject  .          .           .               completed          Completed !?

        no-answer  .          .           .               completed          Completed !?
      missed-call  .          .           .               (same as no-answer)
    """
    client = get_twilio_client()

    call = client.calls.create(
        twiml = f'''
        <Response>
            <Say loop="{loop}">{message}</Say>
        </Response>
        ''',
        to    = to,
        from_ = from_,
    )

    #region tracking call.status until it changes from :queued to succeed/noanswer ie :completed  # another way is to have an api endpoint to register callback ref. https://support.twilio.com/hc/en-us/articles/223132267-Tracking-the-Status-of-an-Outbound-Twilio-Voice-Call
    i=0; n=666  # timeout=666 tick
    while True:
        c = get_call_log.get_by_sid(call.sid)
        print(f'{datetime.now().strftime("%Y%m%d_%H%M%S")} from={c["from"]} to={c["to"]}  status={c["status"]}')
        #       ts                                         .                .             .

        stop = c["status"] not in ['queued', 'ringing', 'in-progress']  # stop by :completed
        if stop: break

        i+=1; stop1 = i>=n
        if stop1: break

        time.sleep(.6)
    #endregion

    print('Done')


def make_call__run_in_background(message, to, from_, loop=0):
    # target run make_call() as background aka thread ref. https://stackoverflow.com/a/7168643/248616
    thr = threading.Thread(target=make_call, name=f'make_call', kwargs=dict(
        message = message,
        to      = to,
        from_   = from_,
        loop    = loop,
    ))
    thr.start()
    #thr.join()  # show message --> this helps print message BUT make threads run in sequel NOT parallel


if __name__ == '__main__':
    import os
    from dotenv import load_dotenv

    load_dotenv()
    make_call(
        message = os.environ.get('MESSAGE_TO_SPEAK'),
        to      = os.environ.get('DEMO_NUMBER_to'),
        from_   = os.environ.get('DEMO_NUMBER_fr'),
        loop    = 0,
    )
