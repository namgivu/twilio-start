import threading
import time
from datetime import datetime

from quickstart.src.service.twilio import get_twilio_client
from quickstart.src import get_call_log


BUSY_RESULT_THRESHOLD = 19  # max tick counted, if still not answered, it should be a BUSY aka no_answer call-result
MAX_RECALL_TIME       = 3   # re-dial maximum times


def make_call_until_answered(message, to, from_, loop=0):
    """
    TODO ask twilio team how to tell if a voice call is answered/rejected/busy since final call status to be always :completed/:Completed? - currently we will do the workaround by counting :ringing_tick
    """
    client = get_twilio_client()

    #region repeat calling until answered
    call_count = 0
    while True:
        # make the voice call
        call = client.calls.create(
            twiml = f'''
            <Response>
                <Say loop="{loop}">{message}</Say>
            </Response>
            ''',
            to    = to,
            from_ = from_,
        )
        call_count+=1

        #region tracking call.status until it changes from :queued to succeed/noanswer ie :completed  # another way is to have an api endpoint to register callback ref. https://support.twilio.com/hc/en-us/articles/223132267-Tracking-the-Status-of-an-Outbound-Twilio-Voice-Call
        i=0; n=666  # timeout=666 tick
        ringing_tick=0  # count how many tick it is
        while True:
            c = get_call_log.get_by_sid(call.sid)

            if c['status'] == 'ringing': ringing_tick+=1

            print(f'ping{i:03d}/call{call_count:02d} {datetime.now().strftime("%Y%m%d_%H%M%S")} from={c["from"]} to={c["to"]}  status={c["status"]} ringing_tick={ringing_tick}')
            #       ts                                         .                .             .

            stop = c["status"] not in ['queued', 'ringing', 'in-progress']  # stop by :completed  # no-answer results at status=status=completed
            if stop: break

            i+=1; stop1 = i>=n
            if stop1: break

            time.sleep(.6)
        #endregion

        #region decide to stop or re-call
        no_answer = ringing_tick >= BUSY_RESULT_THRESHOLD  # BUSY/no_answer --> do re-call
        busy = no_answer
        if not busy:  # call has response, stop calling
            break

        # no answer call --> keep re-call if not reach :MAX_RECALL_TIME
        if call_count>=MAX_RECALL_TIME:
            print(f'Maximum re-call times reached at {MAX_RECALL_TIME}. Give up now.')
            break
        #endregion decide to stop or re-call

    #endregion repeat calling until answered

    print(f'Done calling from={from_} to={to} message={message}')


def make_call_until_answered__run_in_background(message, to, from_, loop=0):
    # target run make_call() as background aka thread ref. https://stackoverflow.com/a/7168643/248616
    thr = threading.Thread(target=make_call_until_answered, name=f'make_call_until_answered', kwargs=dict(
        message = message,
        to      = to,
        from_   = from_,
        loop    = loop,
    ))
    thr.start()
    #thr.join()  # show message --> this helps print message BUT make threads run in sequel NOT parallel


if __name__ == '__main__':
    import os
    from dotenv import load_dotenv

    MODE=os.environ.get('MODE')
    if MODE is None:
        load_dotenv()
        make_call_until_answered(
            message = os.environ.get('MESSAGE_TO_SPEAK'),
            to      = os.environ.get('DEMO_NUMBER_to'),
            from_   = os.environ.get('DEMO_NUMBER_fr'),
            loop    = 0,
        )

    elif MODE=='test_make_call_until_answered__run_in_background__multi_number':
        load_dotenv()

        i=0
        for to in [
            os.environ.get('DEMO_NUMBER_to'),
            os.environ.get('DEMO_NUMBER_to2'),
        ]:
            i+=1
            make_call_until_answered__run_in_background(
                message = os.environ.get('MESSAGE_TO_SPEAK')+f'. Call number {i:02d}',
                to      = to,
                from_   = os.environ.get('DEMO_NUMBER_fr'),
                loop    = 0,
            )
